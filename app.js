const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/routes');
const app = express();
require('./config/db-config');

// Middleware to read body sent as request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Routes Middleware
app.use('/user', routes);

app.listen('3000', () => {
  console.log('Server started at 3000');
});
