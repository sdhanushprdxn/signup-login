const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Creating a schema
const userSchema = new Schema({
  name: { type: String, required: true },
  emailId: { type: String, required: true },
  password: { type: String, required: true }
});

module.exports = mongoose.model('user', userSchema);
