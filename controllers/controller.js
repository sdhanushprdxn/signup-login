const userModel = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const Joi = require('@hapi/joi');

const validateSchema = Joi.object({
  name: Joi.string()
    .min(3)
    .required(),
  emailId: Joi.string()
    .min(6)
    .required()
    .email(),
  password: Joi.string()
    .min(6)
    .required()
});

dotenv.config();
const saltRounds = 10;
module.exports = {
  //sign up controller
  signup: (req, res) => {
    userModel
      .find({ emailId: req.body.emailId })
      .exec()
      .then(user => {
        if (user.length >= 1) {
          return res.status(409).json({
            message: 'User already exits'
          });
        } else {
          // validate data before user is saved
          const { error } = validateSchema.validate({
            name: req.body.name,
            emailId: req.body.emailId,
            password: req.body.password
          });

          if (error)
            return res.status(400).json({ result: error.details[0].message });
          //hash the password
          bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
            if (err) {
              return res.status(500).json({
                error: err
              });
            } else {
              let user = new userModel({
                name: req.body.name,
                emailId: req.body.emailId,
                password: hash
              });
              // save the changes in the db
              user
                .save()
                .then(result => {
                  res.json({
                    success: true,
                    message: 'Signed Up',
                    username: result.name,
                    userEmail: result.emailId
                  });
                })
                .catch(err => {
                  res.status(500).json({ success: false, result: `${err}` });
                });
            }
          });
        }
      });
  },
  //login contoller
  login: (req, res) => {
    userModel
      .findOne({
        $and: [{ name: req.body.name }, { emailId: req.body.emailId }]
      })
      .exec()
      .then(result => {
        //validate user entered password
        bcrypt.compare(req.body.password, result.password, (err, feed) => {
          if (feed) {
            // creating jwt token
            const token = jwt.sign(
              {
                email: result.email,
                userID: result._id
              },
              process.env.ACCESS_SECRET_KEY,
              {
                expiresIn: '1h'
              }
            );
            return res.header('auth-token', token).json({
              status: 'Logged in',
              user: result.name,
              result: 'Enjoy Surfing'
            });
          }
          if (err) {
            return res.json({
              status: 'Log in failed',
              result: feed
            });
          }
        });
      })
      .catch(err => {
        res.status(500).json({
          status: 'Log in failed',
          result: 'Invalid Username Password Combination'
        });
      });
  },
  data: (req, res) => {
    userModel
      .find()
      .exec()
      .then(user => {
        res.json({ user });
      })
      .catch(err => {
        res.status(500).json({
          status: 'Fail',
          result: err
        });
      });
  }
};
