const express = require('express');
const router = express.Router();
const verify = require('../util/verifyToken');
const controller = require('../controllers/controller');

router.get('/', verify, controller.data);

module.exports = router;
