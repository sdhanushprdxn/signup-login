const express = require('express');
const routers = new express.Router();
const signUp = require('./signUp');
const logIn = require('./login');
const data = require('./data');
routers.use('/signup', signUp);
routers.use('/login', logIn);
routers.use('/userdata', data);

module.exports = routers;
